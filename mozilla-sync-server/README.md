# Docker Image of [Mozilla sync server](https://github.com/mozilla-services/syncserver)

This is a Docker image which runs Mozilla sync server under uWSGI on port 9000.

See
[this page](http://docs.blowb.org/install-internet-service-docker/mozilla-sync.html)
for more details.
