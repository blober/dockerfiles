A Docker Image of the CentOS 7 OpenLDAP Server (slapd)
======================================================

Read
[this page](http://docs.blowb.org/install-essential-docker/install-openldap.html)
for more details.
