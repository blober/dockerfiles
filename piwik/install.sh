#!/bin/bash

set -e

cd /var/www

if [ ! -d piwik ] || [ -z "$(ls -A piwik/)" ]; then
  curl -L -s http://builds.piwik.org/piwik.tar.gz | tar zxvf -
fi
