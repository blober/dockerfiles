#!/bin/bash

# install uWSGI plugin(s) and reinstall uWSGI

set -e

cd /usr/src/uwsgi-${UWSGIVERSION}

for plugin in "$@"
do
  python uwsgiconfig.py --plugin plugins/$plugin default
done

python setup.py install
